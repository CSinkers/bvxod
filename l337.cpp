#include <allegro.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "xor.h"

#define OUR_PI 3.14159265358979

#define DegToRad(Deg) (Deg * OUR_PI / 180)
#define RadToDeg(Rad) (Rad * 180 / OUR_PI)

#define SPEEDY_SPEED 10

DATAFILE *GopherDat;
BITMAP *Back;
float x=0,y=0;
float nextx=0, nexty=0;
float oldx=0, oldy=0;
float t=0.0f;
bool isinterping = false;
volatile long KinkyFrameCounterCalledNatashae;

int UserX=0, UserY=0;
int User2X = 0, User2Y=0;

int Level = Donte;

void GoatseTimer()
{
	KinkyFrameCounterCalledNatashae++;
}
END_OF_FUNCTION(GoatseTimer);

void main()
{
	srand((unsigned)time(0));
   long StartTime, EndTime;
   allegro_init();
   install_keyboard();
   install_mouse();
   set_gfx_mode(GFX_VESA1, 1024, 768,0,0);
   Back = create_bitmap(SCREEN_W, SCREEN_H);
   GopherDat = load_datafile("xor.dat");
   if (!GopherDat)
   {
		textprintf(screen, font, 0,0,3,"Could not find xor.dat");
      clear_keybuf();
      readkey();
      return 0x72;
   }
   set_palette((PALLETE)GopherDat[PAL_Norm].dat);
	LOCK_VARIABLE(KinkyFrameCounterCalledNatashae);
	LOCK_FUNCTION(GoatseTimer);
	install_int(GoatseTimer, 1);
  	Level = Donte2;

	while(!(key[KEY_ESC] | key[KEY_Q]))
   {
		StartTime = KinkyFrameCounterCalledNatashae;
		rest(MAX(15-(EndTime-StartTime),0));
		if (key[KEY_W])
         UserY-=SPEEDY_SPEED;
      if (key[KEY_S])
      	UserY+=SPEEDY_SPEED;
      if (key[KEY_A])
      	UserX-=SPEEDY_SPEED;
      if (key[KEY_D])
      	UserX+=SPEEDY_SPEED;
      if (UserX < 0)
	      	UserX = 0;
      if (UserX > SCREEN_W)
      	UserX = SCREEN_W;
      if (UserY < 0)
      	UserY = 0;
      if (UserY > SCREEN_H)
      	UserY = SCREEN_H;

      if (key[KEY_UP])
         User2Y-=SPEEDY_SPEED;
      if (key[KEY_DOWN])
      	User2Y+=SPEEDY_SPEED;
      if (key[KEY_LEFT])
      	User2X-=SPEEDY_SPEED;
      if (key[KEY_RIGHT])
      	User2X+=SPEEDY_SPEED;
      if (User2X < 0)
      	User2X = 0;
      if (User2X > SCREEN_W)
      	User2X = SCREEN_W;
      if (User2Y < 0)
      	User2Y = 0;
      if (User2Y > SCREEN_H)
      	User2Y = SCREEN_H;
      if(!isinterping)
      {
      	nextx = rand()%(SCREEN_W-304);
         nexty = rand()%(SCREEN_H-293);
         oldx = x;
         oldy = y;
     		isinterping = true;
      }
//      KinkyFrameCounterCalledNatashae++;
      //pt = p0 + t(p1 - p0)
      t+=0.01f;
      if(t >= 1.0f)
		{
      	t = 0.0f;
         isinterping = false;
         oldx = x;
         oldy = y;
      }
      x = oldx + t * (nextx - oldx);
      y = oldy + t * (nexty - oldy);

  		if (KinkyFrameCounterCalledNatashae > 200000) {fade_out(1);return 0x724269;}

      if(x == nextx && y == nexty)
      	isinterping = false;

		stretch_sprite(Back, (BITMAP*)GopherDat[Level].dat, 0,0, SCREEN_W, SCREEN_H);
      draw_sprite(Back, (BITMAP*)GopherDat[x02].dat, User2X, User2Y);
      draw_sprite(Back, (BITMAP*)GopherDat[x02].dat, (int)x, (int)y);
      draw_sprite(Back, (BITMAP*)GopherDat[BogWok].dat, UserX, UserY);
      draw_sprite(Back, (BITMAP*)GopherDat[BogWok].dat, mouse_x, mouse_y);
      blit(Back, screen, 0,0,0,0, SCREEN_W, SCREEN_H);
      if (KinkyFrameCounterCalledNatashae == 1)
      	fade_in((PALLETE)GopherDat[PAL_Norm].dat, 1);
	  EndTime = KinkyFrameCounterCalledNatashae;
   }
   destroy_bitmap(Back);
   unload_datafile(GopherDat);
}

